        Requirements Gathering
        It should have a place to store todos
        It should hav a way to display todos
        It should have a way to add new todos
        It should have a way to change todos
        It should have a way to delete todos

        Store todos by creating an array
        var todos = ["item 1", "item 2", "item 3"];

        C Add to an array:
        todos.push("Item 4");

        R Display an array:
        console.log(todo[0]);

        U Change a Todo:
        todo[0] = "Item 1 is updated"

        D Delete a todo:
        todo.splice(0, 1);

        function makeTurkeySandwitch() {
            Get one slide of bread;
            Get some turkey;
            Put a slice of bread on top;
        }

        makeTurkeySandwich();

        function makeSandwichWith(filling){
            return "Get one slide of bread.  " + "Add " + filling + ".  Put a slice of bread on top;";
        }

        makeSandwichWith("ham");

        {
            name: "Phil",
            age: "40"
        }
    -->

    <script>
        var todos = [];

        function displayTodos() {
            console.log(todos);
        }

        displayTodos();

        function addTodos(todo) {
            todos.push(todo);
            displayTodos();
        }

        addTodos("added");

        function changeTodo(position, newValue) {
            todos[position] = newValue;
            displayTodos();
        }

        changeTodo(0, "changed");

        function deleteTodo(position) {
            todos.splice(position, 1);
            displayTodos();
        }

        deleteTodo(0, 1);
    </script>

